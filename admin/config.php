<?php
// HTTP
define('HTTP_SERVER', 'http://aquadesignpro.coms/admin/');
define('HTTP_CATALOG', 'http://aquadesignpro.coms/');

// HTTPS
define('HTTPS_SERVER', 'http://aquadesignpro.coms/admin/');
define('HTTPS_CATALOG', 'http://aquadesignpro.coms/');

// DIR
define('DIR_APPLICATION', 'C:/OSPanel/domains/aquadesignpro.coms/admin/');
define('DIR_SYSTEM', 'C:/OSPanel/domains/aquadesignpro.coms/system/');
define('DIR_IMAGE', 'C:/OSPanel/domains/aquadesignpro.coms/image/');
define('DIR_LANGUAGE', 'C:/OSPanel/domains/aquadesignpro.coms/admin/language/');
define('DIR_TEMPLATE', 'C:/OSPanel/domains/aquadesignpro.coms/admin/view/template/');
define('DIR_CONFIG', 'C:/OSPanel/domains/aquadesignpro.coms/system/config/');
define('DIR_CACHE', 'C:/OSPanel/domains/aquadesignpro.coms/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OSPanel/domains/aquadesignpro.coms/system/storage/download/');
define('DIR_LOGS', 'C:/OSPanel/domains/aquadesignpro.coms/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OSPanel/domains/aquadesignpro.coms/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OSPanel/domains/aquadesignpro.coms/system/storage/upload/');
define('DIR_CATALOG', 'C:/OSPanel/domains/aquadesignpro.coms/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'aquadesignpro');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
